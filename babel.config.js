module.exports = function (api) {
  api.cache(true);

  const presets = [
    '@babel/preset-env',
    '@babel/preset-react',
  ];
  const plugins = [
    '@babel/plugin-proposal-class-properties',
    '@babel/plugin-proposal-export-default-from',
    '@babel/plugin-transform-runtime',
    ['css-modules-transform', {
      "extensions": [".css", ".scss"],
    }],
  ];

  return {
    presets,
    plugins,
  };
};
