const merge = require('webpack-merge');

const ENV = process.env.NODE_ENV || 'development';

const base = require('./webpack/webpack.base.conf'); // eslint-disable-line import/newline-after-import
const conf = require(`./webpack/webpack.${ENV}.conf`);

module.exports = (env = {}) => merge(base(env), conf(env));
