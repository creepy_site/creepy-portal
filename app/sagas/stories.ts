import {
  put,
  takeEvery,
  all,
  call,
  getContext,
} from 'redux-saga/effects';

import {
  fetchStory,
  fetchStories,
  fetchStoriesCount,
  fetchStoriesByTag,
  requestStoryLike,
  requestInitialData,
  appendComment,
} from '@fetch/stories';

import {
  REQUEST_STORY,
  REQUEST_STORIES,
  SHOW_PAGINATION,
  REQUEST_STORIES_BY_TAG,
  REQUEST_LIKE,
  REQUEST_DISLIKE,
  REQUEST_INITIAL_DATA_FOR_USER,
  APPEND_COMMENT,
} from '../actions';
import {
  genericStartAC,
  genericSuccessAC,
  genericFailAC,
} from '../actions/utils/actionGeneric';

const HTTP_NO_CONTENT = 204;

function* callFetchStories(action: any) {
  const {
    query,
    offset = 1,
  } = action.payload;
  yield put(genericStartAC(REQUEST_STORIES));

  const transport = yield getContext('transport');
  const [stories, count] : [any, any] = yield all([
    fetchStories(transport, query, offset),
    fetchStoriesCount(transport),
  ]);

  if (stories && count) {
    yield put({ type: SHOW_PAGINATION });
    yield put(genericSuccessAC(REQUEST_STORIES, { stories, count }));
  } else {
    yield put(genericFailAC(REQUEST_STORIES));
  }
}

function* callFetchStory(action: any) {
  yield put(genericStartAC(REQUEST_STORY));

  const transport = yield getContext('transport');
  const story = yield call(fetchStory, transport, action.payload.token, action.payload.id);

  if (story) {
    yield put(genericSuccessAC(REQUEST_STORY, { story }));
  } else {
    yield put(genericFailAC(REQUEST_STORY));
  }
}

function* callFetchStoriesByTag(action: any) {
  const { tag } = action.payload;

  const transport = yield getContext('transport');
  yield put(genericStartAC(REQUEST_STORIES_BY_TAG));

  const stories = yield fetchStoriesByTag(transport, tag);
  if (stories) {
    yield put(genericSuccessAC(REQUEST_STORIES_BY_TAG, { stories }));
  } else {
    yield put(genericFailAC(REQUEST_STORIES_BY_TAG));
  }
}

function* callRequsetLike(action: any) {
  const { uID } = action.payload;

  const transport = yield getContext('transport');
  yield put(genericStartAC(REQUEST_LIKE));

  const response = yield call(requestStoryLike, transport, uID, 'like');
  if (response && response.status === HTTP_NO_CONTENT) {
    yield put(genericSuccessAC(REQUEST_LIKE, { uID }));
  } else {
    yield put(genericFailAC(REQUEST_LIKE));
  }
}

function* callRequsetDislike(action: any) {
  const { uID } = action.payload;

  const transport = yield getContext('transport');
  yield put(genericStartAC(REQUEST_DISLIKE));

  const response = yield call(requestStoryLike, transport, uID, 'unlike');
  if (response.status === HTTP_NO_CONTENT) {
    yield put(genericSuccessAC(REQUEST_DISLIKE, { uID }));
  } else {
    yield put(genericFailAC(REQUEST_DISLIKE));
  }
}

function* callRequestInitialData(action: any) {
  const { uIDs } = action.payload;

  const transport = yield getContext('transport');
  yield put(genericStartAC(REQUEST_INITIAL_DATA_FOR_USER));

  const initialData = yield call(requestInitialData, transport, uIDs);
  if (initialData) {
    yield put(genericSuccessAC(REQUEST_INITIAL_DATA_FOR_USER, { initialData }));
  } else {
    yield put(genericFailAC(REQUEST_INITIAL_DATA_FOR_USER));
  }
}

function* callAppendComment(action: any) {
  const { uID, msg } = action.payload;

  const transport = yield getContext('transport');
  yield put(genericStartAC(APPEND_COMMENT));

  const comment = yield call(appendComment, transport, uID, msg);
  if (comment) {
    yield put(genericSuccessAC(APPEND_COMMENT, { comment, uID }));
  } else {
    yield put(genericFailAC(APPEND_COMMENT));
  }
}

export default function* watchStories() {
  yield takeEvery(REQUEST_STORIES, callFetchStories);
  yield takeEvery(REQUEST_STORY, callFetchStory);
  yield takeEvery(REQUEST_STORIES_BY_TAG, callFetchStoriesByTag);
  yield takeEvery(REQUEST_LIKE, callRequsetLike);
  yield takeEvery(REQUEST_DISLIKE, callRequsetDislike);
  yield takeEvery(REQUEST_INITIAL_DATA_FOR_USER, callRequestInitialData);
  yield takeEvery(APPEND_COMMENT, callAppendComment);
}
