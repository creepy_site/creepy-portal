import {
  put,
  takeEvery,
  call,
  getContext,
} from 'redux-saga/effects';
import { AxiosInstance } from 'axios';

import {
  postStory,
  requestUserInfo,
  requestPubInfoOfUser,
} from '@fetch/users';

import {
  REQUEST_USER_ADD_STORY,
  REQUEST_USER_INFO,
  REQUEST_USER_PUB_INFO,
} from '../actions';
import {
  genericStartAC,
  genericSuccessAC,
  genericFailAC,
} from '../actions/utils/actionGeneric';

function* callPostStory(action: any) {
  const transport: AxiosInstance = yield getContext('transport');

  yield put(genericStartAC(REQUEST_USER_ADD_STORY));
  const { title, content } = action.payload;
  // TODO: check to store has user data before send story
  const answer = yield call(postStory, transport, title, content);
  if (answer) {
    yield put(genericSuccessAC(REQUEST_USER_ADD_STORY));
  } else {
    yield put(genericFailAC(REQUEST_USER_ADD_STORY));
  }
}

function* callRequestUserInfo(action: any) {
  const transport: AxiosInstance = yield getContext('transport');

  yield put(genericStartAC(REQUEST_USER_INFO));
  const { username } = action.payload;
  const userData = yield call(requestUserInfo, transport, username);

  if (userData) {
    yield put(genericSuccessAC(REQUEST_USER_INFO, { userData }));
  } else {
    yield put(genericFailAC(REQUEST_USER_INFO));
  }
}

function* callRequestPubInfoOfUser(action: any) {
  const transport: AxiosInstance = yield getContext('transport');

  yield put(genericStartAC(REQUEST_USER_PUB_INFO));
  const { username } = action.payload;
  const userPubInfo = yield call(requestPubInfoOfUser, transport, username);

  if (userPubInfo) {
    yield put(genericSuccessAC(REQUEST_USER_PUB_INFO, { userPubInfo }));
  } else {
    yield put(genericFailAC(REQUEST_USER_PUB_INFO));
  }
}

export default function* watchStories() {
  yield takeEvery(REQUEST_USER_ADD_STORY, callPostStory);
  yield takeEvery(REQUEST_USER_INFO, callRequestUserInfo);
  yield takeEvery(REQUEST_USER_PUB_INFO, callRequestPubInfoOfUser);
}
