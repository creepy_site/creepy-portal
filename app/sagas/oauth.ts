import {
  put,
  select,
  takeEvery,
  getContext,
} from 'redux-saga/effects';
import axios from 'axios';

import {
  fetchAuth,
  fetchRegister,
  requestLogout,
} from '@fetch/oauth';

import {
  REQUEST_AUTH,
  REQUEST_REG,
  REQUEST_LOGOUT,
  requestInitialAC,
  closeDialogAC,
  getUIDsOfStories,
} from '../actions';
import {
  genericStartAC,
  genericSuccessAC,
  genericFailAC,
} from '../actions/utils/actionGeneric';

function* callRequestAuth(action: any) {
  const {
    login,
    password,
  } = action.payload;

  const transport = yield getContext('transport');

  yield put(genericStartAC(REQUEST_AUTH));

  const authData = yield fetchAuth(transport, login, password);
  if (authData) {
    yield put(genericSuccessAC(REQUEST_AUTH, { authData }));
    const uIDs = yield select(getUIDsOfStories);

    if (authData.token) {
      transport.defaults.headers.common.Authorization = `Bearer ${authData.token}`;
      console.log('set token', transport.defaults.headers.common.Authorization);
    }
    if (uIDs.length > 0) {
      yield put(requestInitialAC(uIDs));
    }
    yield put(closeDialogAC());
  } else {
    yield put(genericFailAC(REQUEST_AUTH));
  }
}

function* callRequestReg(action: any) {
  const transport = yield getContext('transport');

  yield put(genericStartAC(REQUEST_REG));

  const regData = yield fetchRegister(transport, action.payload);
  if (regData) {
    yield put(genericSuccessAC(REQUEST_REG, { regData }));
    yield put(closeDialogAC());
    if (regData.token) {
      axios.defaults.headers.common.Authorization = `Bearer ${regData.token}`;
      console.log('set token', axios.defaults.headers.common.Authorization);
    }
  } else {
    yield put(genericFailAC(REQUEST_REG, { regData }));
  }
}

function* callRequestLogout() {
  const transport = yield getContext('transport');

  yield put(genericStartAC(REQUEST_LOGOUT));

  const logoutData = yield requestLogout(transport);
  if (logoutData) {
    yield put(genericSuccessAC(REQUEST_LOGOUT));
  } else {
    yield put(genericFailAC(REQUEST_LOGOUT));
  }
}

export default function* watchOauth() {
  yield takeEvery(REQUEST_AUTH, callRequestAuth);
  yield takeEvery(REQUEST_REG, callRequestReg);
  yield takeEvery(REQUEST_LOGOUT, callRequestLogout);
}
