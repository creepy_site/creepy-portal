import { takeEvery, put, getContext } from 'redux-saga/effects';

import { fetchAllTags, fetchTagInfo } from '@fetch/tags';
import { REQUEST_ALL_TAGS, REQUEST_TAG_INFO } from '../actions';
import {
  genericStartAC,
  genericSuccessAC,
  genericFailAC,
} from '../actions/utils/actionGeneric';

function* callFetchAllTags() {
  const transport = yield getContext('transport');

  yield put(genericStartAC(REQUEST_ALL_TAGS));

  const tags = yield fetchAllTags(transport);

  if (tags) {
    yield put(genericSuccessAC(REQUEST_ALL_TAGS, { tags }));
  } else {
    yield put(genericFailAC(REQUEST_ALL_TAGS));
  }
}

function* callFetchTagInfo(action: any) {
  const { payload: { tag } } = action;

  const transport = yield getContext('transport');

  yield put(genericStartAC(REQUEST_TAG_INFO));

  const tagInfo = yield fetchTagInfo(transport, tag);

  if (tagInfo) {
    yield put(genericSuccessAC(REQUEST_TAG_INFO, { tagInfo }));
  } else {
    yield put(genericFailAC(REQUEST_TAG_INFO));
  }
}

export default function* watchTags() {
  yield takeEvery(REQUEST_ALL_TAGS, callFetchAllTags);
  yield takeEvery(REQUEST_TAG_INFO, callFetchTagInfo);
}
