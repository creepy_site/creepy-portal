import {
  put,
  takeEvery,
  call,
  getContext,
} from 'redux-saga/effects';

import { uploadAvatarImage } from '@fetch/users';
import {
  UPLOAD_AVATAR_IMAGE,
  closeDialogAC,
} from '../actions';
import {
  genericStartAC,
  genericSuccessAC,
  genericFailAC,
} from '../actions/utils/actionGeneric';

function* callUploadAvatarImage(action: any) {
  const transport = yield getContext('transport');

  yield put(genericStartAC(UPLOAD_AVATAR_IMAGE));
  const { file } = action.payload;

  const accountImage = yield call(uploadAvatarImage, transport, file);
  if (accountImage) {
    yield put(genericSuccessAC(UPLOAD_AVATAR_IMAGE, { accountImage }));
    yield put(closeDialogAC());
  } else {
    yield put(genericFailAC(UPLOAD_AVATAR_IMAGE));
  }
}

export default function* watchStories() {
  yield takeEvery(UPLOAD_AVATAR_IMAGE, callUploadAvatarImage);
}
