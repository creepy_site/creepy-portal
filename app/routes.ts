import StoriesPage from './pages/stories';
import ScaryPage from './pages/scary';
import StoryPage from './pages/story';
import RandomPage from './pages/random';

// TODO: move to utils
import privateRoute from './containers/PrivateRoute/privateRoute';
import TagsContainer from './containers/Tags/TagsContainer';
import TagContentContainer from './containers/TagContent/TagContentContainer';
import UserPageContainer from './containers/UserPage/UserPageContainer';
import UserStoryContainer from './containers/UserStory/UserStoryContainer';

import About from '@routes/about';
import NotFound from '@routes/not-found';

export default [
  {
    path: '/',
    exact: true,
    component: StoriesPage,
  },
  {
    path: '/stories/',
    exact: true,
    component: StoriesPage,
  },
  {
    path: '/stories/:page',
    component: StoriesPage,
  },
  {
    path: '/scary/',
    component: ScaryPage,
    exact: true,
  },
  {
    path: '/scary/:page',
    component: ScaryPage,
    exact: true,
  },
  {
    path: '/story/:id',
    component: StoryPage,
    exact: true,
  },
  {
    path: '/random/',
    component: RandomPage,
    exact: true,
  },
  {
    path: '/about/',
    component: About,
    exact: true,
  },
  {
    path: '/tags/',
    component: TagsContainer,
    exact: true,
  },
  {
    path: '/tags/:tag',
    component: TagContentContainer,
    exact: true,
  },
  {
    path: '/user/:username',
    component: UserPageContainer,
    exact: true,
  },
  {
    path: '/new',
    // @ts-ignore
    component: privateRoute(UserStoryContainer),
    exact: true,
  },
  {
    path: '*',
    component: NotFound,
  },
];
