import React, { Component } from 'react';
import { Helmet } from 'react-helmet';
import MediaQuery from 'react-responsive';

import Pagination from './parts/Pagination';
import StoriesList from './parts/StoriesList';
import Greeting from './parts/Greeting';
import SidebarContainer from '../../../containers/Sidebar/SidebarContainer';

const DESC = {
  stories: 'Собрание страшилок и страшных историй на основе реальных событий из разных уголков мира.',
  scary: 'Собрание страшилок и страшных историй на основе реальных событий из разных уголков мира, показанные по популярности.',
  random: 'Случайная страшилка или страшная история. Каждый раз новая.',
};

const getDescriptionByToken = (token) => DESC[token];

class StoryList extends Component {
  render() {
    const {
      token,
      stories,
      helloMessage,
      showPagination,
      storiesTotal,
      currentPage,
    } = this.props;

    // TODO: please no literals
    const maxPages = Math.ceil(storiesTotal / 10);
    return (
      <div>
        <Helmet>
          <title>Страшные истории</title>
          <meta name='Description' content={getDescriptionByToken(token)}/>
        </Helmet>
        {helloMessage
          && <Greeting/>
        }
        {/* // TODO: make without query param. withRouter may helps alot */}
        {showPagination
          && <Pagination maxPages={maxPages} currentPage={currentPage} query={token}/>
        }
        { (stories && stories.length !== 0)
          && <div className="page-content">
            <StoriesList
              stories={stories}
            />
            {/* Hide sidebar to small devices */}
            <MediaQuery minWidth={650}>
              <SidebarContainer />
            </MediaQuery>
          </div>
        }
        <div style={{ clear: 'both' }}></div>
        {/* // TODO: make without query param. withRouter may helps alot */}
        {showPagination
          && <Pagination maxPages={maxPages} currentPage={currentPage} query={token}/>
        }
      </div>
    );
  }
}

export default StoryList;
