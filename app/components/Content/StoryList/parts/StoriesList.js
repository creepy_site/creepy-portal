import React from 'react';

import Story from '../../Story';

class StoriesList extends React.Component {
  render() {
    const {
      stories,
    } = this.props;

    return (
      <div className={'content'}>
        {stories.length > 0
          ? stories.map(story => (
            <Story
              story={story}
              key={story.uID}
            />
          ))
          : <div className="panel-top">
            <h1>Ничего нет</h1>
            <p>По данной ссылке нет историй.</p>
          </div>
        }
      </div>
    );
  }
}

export default StoriesList;
