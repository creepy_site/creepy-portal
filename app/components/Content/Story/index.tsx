import React, { Component } from 'react';
import { connect } from 'react-redux';
import { Helmet } from 'react-helmet';
import PropTypes from 'prop-types';

import StoryContent from './parts/StoryContent';
import StoryControls from './parts/StoryControls';
import CommentsBlockContainer from '../../../containers/CommentsBlock/CommentsBlockContainer';
import withCurrentUser from '../../../containers/withCurrentUser/withCurrentUser';
import { AUTH_MODAL_ITEM } from '../../AuthModal/AuthModal';
import {
  requestLikeAC,
  requestDislikeAC,
  isUserAuthorized,
  openDialogAC,
  getCurrentOauthData,
} from '../../../actions';

class StoryItem extends Component {
  handleLike = () => {
    const {
      isAuth,
      requestLikeAC,
      requestDislikeAC,
      openDialogAC,
      story,
    } = this.props;

    const { uID } = story;
    const isLikeWasSet = story.wasLiked || false;

    if (!isAuth) {
      return openDialogAC(AUTH_MODAL_ITEM);
    }

    if (!isLikeWasSet) {
      requestLikeAC(uID);
    } else {
      requestDislikeAC(uID);
    }
  };

  render() {
    const {
      story,
      activeTag,
      verbose,
      oauth,
    } = this.props;

    if (!story) {
      return null;
    }

    const isLikeWasSet = story.wasLiked || false;
    const {
      uID,
      commentsCount,
      likesCount,
      storyTitle,
    } = story;

    const STATUSES = [
      'В очереди на рассмотрение',
      'Отклонена',
      'В рассмотрении',
      'Опубликована',
    ];

    const currentStatus = story.data.status;

    return (
      <div className="story-item">
        <Helmet>
          <title>{`${story.title} - Страшные истории`}</title>
          <meta name="Title" content={`${story.title} - Страшные истории`}/>
          <meta name="Description" content={story.data.description}/>
        </Helmet>
        {(story.author && story?.author?.username === oauth?.get('user')) && currentStatus !== 3 && (
          <div style={{ margin: '10px 0', padding: '10px', background: 'white' }}>
            Текущий статус истории: {STATUSES[currentStatus]}
          </div>
        )}
        <StoryContent
          story={story}
          activeTag={activeTag}
        />
        <StoryControls
          handleLike={this.handleLike}
          hasCommentsIcon={!verbose}
          isLikeWasSet={isLikeWasSet}
          uID={uID}
          commentsCount={commentsCount}
          likesCount={likesCount}
          storyTitle={storyTitle}
        />
        { verbose && <CommentsBlockContainer story={story} />}
      </div>
    );
  }
}

StoryItem.propTypes = {
  story: PropTypes.instanceOf(Map).isRequired,
  isAuth: PropTypes.bool.isRequired,
  requestLikeAC: PropTypes.func.isRequired,
  requestDislikeAC: PropTypes.func.isRequired,
  openDialogAC: PropTypes.func.isRequired,
  user: PropTypes.string,
  activeTag: PropTypes.string,
  verbose: PropTypes.bool,
};

StoryItem.defaultProps = {
  activeTag: null,
  verbose: false,
  user: null,
};

export default connect(state => ({
  isAuth: isUserAuthorized(state),
  oauth: getCurrentOauthData(state),
}), {
  requestLikeAC,
  requestDislikeAC,
  openDialogAC,
})(withCurrentUser(StoryItem));
