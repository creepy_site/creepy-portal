import React, { FC } from 'react';
import { Helmet } from 'react-helmet';
import { useSelector } from 'react-redux';
import MediaQuery from 'react-responsive';
import { withRouter } from 'react-router';
import { Route, Switch } from 'react-router-dom';

import NotFound from '@routes/not-found';
import About from '@routes/about';
import RandomPage from '@routes/random';
import TagContent from '@routes/tag-content';
import StoriesPage from '@routes/stories';
import Story from '@routes/story';
import ScaryPage from '@routes/scary';
import UserStory from '@routes/user-story';
import TagsPage from '@routes/tags';
import UserPage from '@routes/user';

import Logo from './parts/logo';
import Footer from '@components/footer';
import MobileSidebar from '../../containers/MobileSidebar/MobileSidebar';
import NavbarContainer from '../../containers/Navbar/NavbarContainer';
import DialogContainer from '../../containers/Dialog/DialogContainer';

import privateRoute from '@utils/private-route';
// import TagContent from '../../containers/TagContent/TagContentContainer';

import './app.css';
import './styles/navbar.css';
import './styles/mobile-sidebar.css';
import './styles/footer.css';
import './styles/auth-message-box.css';
import './styles/user-homepage.css';
import './styles/user-page.css';
import './styles/content/greeting.css';
import './styles/content/pagination.css';
import './styles/content/content.css';
import './styles/content/story-item.css';
import './styles/content/comments-block.css';
import './styles/sidebar/sidebar.css';
import './styles/sidebar/page-tags-sidebar-module.css';
import './styles/sidebar/auth-sidebar-module.css';
import './styles/sidebar/user-sidebar-module.css';

const App: FC<{}> = () => {
  const logoNumber = useSelector((state) => state.app.logoNumber);

  return (
    <div className="root">
      <Helmet>
        <title>Страшные истории</title>
        <meta name="Description" content="Собрание страшилок и страшных историй на основе реальных событий из разных уголков мира. Возможность поделиться своей историей." />
        <meta name="Keywords" content="страшилки,страшные истории, мистические истории, мистика, на ночь, ужасные истории, страх, ужас, ужастики" />
      </Helmet>
      <div className="root__wrap">
        <Logo logoNumber={logoNumber} />
        <NavbarContainer />
        <div className="wrap main">
          <Switch>
            <Route path="/" exact component={() => <StoriesPage helloMessage />} />
            <Route path="/stories/" exact component={StoriesPage} />
            <Route path="/stories/:page" exact component={StoriesPage} />
            <Route path="/scary/" exact component={ScaryPage} />
            <Route path="/scary/:page" exact component={ScaryPage} />
            <Route path="/story/:id" exact component={Story} />
            <Route path="/random/" exact component={RandomPage} />
            <Route path="/about/" exact component={About} />
            <Route path="/tags" exact component={TagsPage} />
            <Route path="/tags/:tag" exact component={TagContent} />
            <Route path="/user/:username" exact component={UserPage} />
            <Route path="/new" exact component={privateRoute(UserStory)} />
            <Route component={NotFound} />
          </Switch>
        </div>
        {/* Enable mobiles sidebar for low resolution devices */}
        <MediaQuery maxWidth={650}>
          <MobileSidebar />
        </MediaQuery>
        <DialogContainer />
      </div>
      <Footer />
    </div>
  );
};

export default withRouter(App);
