import React, { FC } from 'react';
import { Link } from 'react-router-dom';

const Logo: FC<{ logoNumber: Number }> = ({ logoNumber }) => (
  <div className="top">
    <div className="wrap header">
      <Link to="/" title="Страшные истории">
        <img className="scary-label" alt="logo" src="/images/logo.png" />
      </Link>
      <Link to="/">
        <img src={`/images/lg/${logoNumber}.png`} alt="logo-image" className="ghost-logo"/>
      </Link>
    </div>
  </div>
);

export default Logo;
