import { createStore, applyMiddleware, compose } from 'redux';
import createSagaMiddleware, { END } from 'redux-saga';
import { fromJS } from 'immutable';
import { get } from 'lodash';
import axios from 'axios';
import { createLogger } from 'redux-logger';
import { ApplicationState } from '@reducers/index';

import reducer from '../reducers';

const snapshot: Partial<ApplicationState> = {};

if (typeof window !== 'undefined') {
  const rawData = JSON.parse(decodeURIComponent(window.escape(atob(window.__snapshot__))));
  const token = get(rawData, 'oauth.auth.user.token');

  /* Set access token to request head */
  if (token) {
    axios.defaults.headers.common.Authorization = `Bearer ${token}`;
    console.log(axios.defaults.headers.common.Authorization); // eslint-disable-line no-console
  }

  Object.keys(rawData).forEach((key) => {
    if (!['tags', 'stories', 'app', 'dialog', 'users'].includes(key)) {
      snapshot[key] = fromJS(rawData[key]);
    } else {
      snapshot[key] = rawData[key];
    }
  });
}

export default function configureStore(transport, initialState = snapshot) {
  const sagaMiddleware = createSagaMiddleware();
  const middlewares = [applyMiddleware(sagaMiddleware)];

  const logger = createLogger({});

  middlewares.push(applyMiddleware(logger));

  if (typeof window !== 'undefined' && window.__REDUX_DEVTOOLS_EXTENSION__) {
    middlewares.push(window.__REDUX_DEVTOOLS_EXTENSION__());
  }

  const store = createStore(
    reducer,
    initialState,
    compose(...middlewares),
  );

  store.runSaga = sagaMiddleware.run.bind(sagaMiddleware);
  sagaMiddleware.setContext({ transport });
  store.close = () => store.dispatch(END);
  return store;
}
