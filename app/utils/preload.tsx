import React, { Component } from 'react';
import { Dispatch } from 'redux';
import { connect } from 'react-redux';

interface FetchParams {
  dispatch: Dispatch;
  [key: string]: any;
}

interface Params {
  fetch: (params: FetchParams) => any
}

export default function withDataPreload({ fetch }: Params, WrappedComponent: React.ComponentClass | React.FC<any>) {
  class PreloadHoc extends Component {
    constructor(props) {
      super(props);

      if (__SERVER__) {
        fetch(this.props);
      }
    }

    componentDidMount() {
      if (this.props.initStep) {
        return;
      }

      fetch(this.props);
    }

    render() {
      return (<WrappedComponent {...this.props} />);
    }
  }

  return connect((state) => ({ initStep: state.app.initStep }))(PreloadHoc);
}
