import { useRef } from 'react';

export const useComponentWillMount = (func) => {
  const willMount = useRef(true);
  // debugger;
  if (willMount.current) {
    func();
  }

  willMount.current = false;
};

export default {
  useComponentWillMount,
};
