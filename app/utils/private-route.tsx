import React, { ComponentClass } from 'react';
import { Route, Redirect } from 'react-router-dom';
import { useSelector } from 'react-redux';
import { isNull } from 'lodash';

import { getCurrentUserUsername } from '@actions';

function privateRoute<T>(Component: ComponentClass<T>, redirrectLocation = '/') {
  const renderOrRedirect = (user: string, props: T) => (
    !isNull(user)
      ? <Component {...props} />
      : <Redirect to={redirrectLocation} />
  );

  const Private = (props: T) => {
    const user = useSelector(getCurrentUserUsername);

    return (
      <Route
        {...props}
        render={(componentProps: any) => renderOrRedirect(user, componentProps)}
      />
    );
  }

  return Private;
};

export default privateRoute;
