export default function (param: string = 'ENV') {
  try {
    if (__SERVER__) {
      return process.env[`NODE_${param}`];
    }
    
    if (__CLIENT__) {
      return window[`__${param}__`];
    }
  } catch (e) {
    console.warn(`Request ${param} variable doesn't set`);
    return null;
  }
}
