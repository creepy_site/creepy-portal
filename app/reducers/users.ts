import {
  REQUEST_USER_INFO,
  REQUEST_USER_ADD_STORY,
  REQUEST_USER_PUB_INFO,
  UPLOAD_AVATAR_IMAGE,
  REFRESH_USER_UPLOAD_STATE,
} from '@actions';
import {
  START,
  FAIL,
  SUCCESS,
} from '../actions/baseActions';

// export interface UsersState {
//   user: null,
//   userPubInfo: [],
//   userStory: {
//     data: {
//       title: '',
//       content: '',
//     },
//     state: { ...loadingState },
//   },
//   upload: {
//     error: false,
//   },
// }

const loadingState = {
  loading: false,
  success: false,
  fail: false,
};

const initState = {
  user: null,
  userPubInfo: [],
  userStory: {
    data: {
      title: '',
      content: '',
    },
    state: { ...loadingState },
  },
  upload: {
    error: false,
  },
};

export default function usersReducer(state = initState, action) {
  switch (action.type) {
    case REQUEST_USER_INFO + SUCCESS: {
      const { userData } = action.payload;

      return {
        ...state,
        user: userData,
      }
    }
    case REQUEST_USER_ADD_STORY + START:
      return {
        ...state,
        userStory: {
          ...state.userStory,
          state: loadingState
        }
      }
    case REQUEST_USER_ADD_STORY + SUCCESS:
      return {
        ...state,
        userStory: {
          ...state.userStory,
          state: {
            success: true,
            loading: false,
            fail: false
          }
        }
      }
    case REQUEST_USER_ADD_STORY + FAIL:
      return {
        ...state,
        userStory: {
          ...state.userStory,
          state: {
            success: false,
            loading: false,
            fail: true
          }
        }
      }
    case REQUEST_USER_PUB_INFO + SUCCESS: {
      const { userPubInfo } = action.payload;
      return {
        ...state,
        userPubInfo
      }
    }
    case UPLOAD_AVATAR_IMAGE + SUCCESS: {
      const { accountImage } = action.payload;
      return {
        ...state,
        user: {
          ...state.user,
          accountImage
        }
      }
    }
    case UPLOAD_AVATAR_IMAGE + FAIL: 
      return {
        ...state, 
        upload: {
          ...state.upload,
          error: true
        }
      }
    case REFRESH_USER_UPLOAD_STATE:
      return {
        ...state,
        upload: {
          ...state.upload,
          error: false
        }
      }
    default:
      return state;
  }
}
