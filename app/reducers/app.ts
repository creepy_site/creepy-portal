import set from 'lodash/fp/set';
import {
  SET_LOGO_NUM,
  OPEN_SIDEBAR,
  CLOSE_SIDEBAR,
  INIT_DONE,
} from '../actions';

export interface AppState {
  logoNumber: number;
  loading: boolean;
  initStep: boolean;
  sidebar: {
    isOpen: boolean;
  };
}

const initState = {
  logoNumber: -1,
  loading: false,
  initStep: true,
  sidebar: {
    isOpen: false,
  },
};

export default function appReducer(state = initState, action: any) {
  switch (action.type) {
    case INIT_DONE:
      return set('initStep', false, state);
    case OPEN_SIDEBAR:
      return set('sidebar.isOpen', true, state);
    case CLOSE_SIDEBAR:
      return set('sidebar.isOpen', false, state);
    case SET_LOGO_NUM:
      return set('logoNumber', action.payload.number, state);
    default:
      return state;
  }
}
