import {
  SHOW_DIALOG,
  HIDE_DIALOG,
} from '../actions';

export interface DialogState {
  isOpen: boolean;
  content: any;
}

const initState = {
  isOpen: false,
  content: null,
};

export default function dialogReducer(state = initState, action: any) {
  switch (action.type) {
    case SHOW_DIALOG:
      return {
        ...state,
        isOpen: true,
        content: action.payload.content,
      };
    case HIDE_DIALOG:
      return {
        ...state,
        isOpen: false,
      };
    default:
      return state;
  }
}
