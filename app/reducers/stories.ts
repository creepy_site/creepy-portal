import set from 'lodash/fp/set';
import pipe from 'lodash/fp/pipe';

import { find } from 'lodash';
import {
  REQUEST_STORY,
  CLEAR_STORY,
  CLEAR_STORIES,
  REQUEST_STORIES,
  REQUEST_LIKE,
  REQUEST_DISLIKE,
  REQUEST_INITIAL_DATA_FOR_USER,
  APPEND_COMMENT,
  REQUEST_LOGOUT,
} from '../actions';
import {
  START,
  SUCCESS,
  FAIL,
} from '../actions/baseActions';

const initState = {
  entries: [],
  story: null,
  loading: true, // TODO: prevent force move to 404
  showPagination: false,
  count: -1,
};

const setLikeOrDislike = (story, uID, action) => {
  const isLike = action === 'like';
  const storyID = story.uID;
  const { likesCount } = story;

  if (storyID !== uID) {
    return story;
  }

  return {
    ...story,
    wasLiked: isLike,
    likesCount: isLike ? likesCount + 1 : likesCount - 1,
  };
};

export default function storiesReducer(state = initState, action) {
  switch (action.type) {
    case CLEAR_STORY:
      return pipe(
        (v) => set('story', null, v),
        (v) => set('loading', true, v),
      )(state);

    case CLEAR_STORIES:
      return pipe(
        (v) => set('entries', [], v),
        (v) => set('count', -1, v),
      )(state);

    case REQUEST_STORIES + SUCCESS: {
      const {
        stories,
        count,
      } = action.payload;

      const showPagination = stories.length > 1;

      return pipe(
        (v) => set('entries', stories, v),
        (v) => set('count', count, v),
        (v) => set('loading', false, v),
        (v) => set('showPagination', showPagination, v),
      )(state);
    }

    case REQUEST_STORIES + START:
    case REQUEST_STORY + START:
      return set('loading', true, state);

    case REQUEST_STORIES + FAIL:
    case REQUEST_STORY + FAIL:
      return set('loading', false, state);

    case REQUEST_STORY + SUCCESS: {
      const {
        story,
      } = action.payload;

      return pipe(
        (v) => set('story', story, v),
        (v) => set('loading', false, v),
        (v) => set('showPagination', false, v),
      )(state);
    }

    case REQUEST_LIKE + SUCCESS: {
      const { uID } = action.payload;

      const entries = state.entries.map((story) => setLikeOrDislike(story, uID, 'like'));
      let { story } = state;

      if (story) {
        story = setLikeOrDislike(story, uID, 'like');
      }

      return pipe(
        (v) => set('story', story, v),
        (v) => set('entries', entries, v),
      )(state);
    }

    case REQUEST_DISLIKE + SUCCESS: {
      const { uID } = action.payload;

      const entries = state.entries.map((story) => setLikeOrDislike(story, uID, 'dislike'));
      let { story } = state;

      if (story) {
        story = setLikeOrDislike(story, uID, 'dislike');
      }

      return pipe(
        (v) => set('story', story, v),
        (v) => set('entries', entries, v),
      )(state);
    }

    case REQUEST_INITIAL_DATA_FOR_USER + SUCCESS: {
      const { initialData } = action.payload;

      // todo temp
      let newState = JSON.parse(JSON.stringify(state));

      const entries = newState.entries.map((story) => {
        const uID = story.uID;
        const storyInfo = find(initialData, { uID });

        story.wasLiked = storyInfo.isLiked;

        return story;
      });

      newState.entries = entries;

      const story = state.story;

      if (story) {
        const uID = story.uID;
        const storyInfo = find(initialData, { uID });
        newState.story.wasLiked = storyInfo.isLiked;
      }
      return newState;
    }

    case APPEND_COMMENT + SUCCESS: {
      const { comment } = action.payload;

      const commments = state.story.comments.concat(comment);

      return set('story.comments', commments, state);
    }

    case REQUEST_LOGOUT + SUCCESS: {
      return {
        ...state,
        entries: state.entries.map((story) => set('wasLiked', false, story)),
        story: set('wasLiked', false, state.story),
      };
    }

    default:
      return state;
  }
}
