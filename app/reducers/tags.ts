import { SUCCESS } from '../actions/baseActions';
import {
  REQUEST_STORIES_BY_TAG,
  REQUEST_ALL_TAGS,
  REQUEST_TAG_INFO,
  CLEAR_TAG_INFO,
} from '../actions';

export interface TagsState {
  list: Array<string>;
  stories: Array<any>; // TODO
  tagInfo: any; // TODO
  showPagination: boolean;
  count: number;
}

const initState = {
  list: [],
  stories: [],
  tagInfo: null,
  showPagination: false,
  count: -1,
};

export default function tagsReducer(state = initState, action: any) {
  switch (action.type) {
    case REQUEST_ALL_TAGS + SUCCESS: {
      const { tags } = action.payload;

      return {
        ...state,
        list: tags,
      };
    }

    case REQUEST_STORIES_BY_TAG + SUCCESS: {
      const { stories } = action.payload;

      return {
        ...state,
        stories,
      };
    }

    case REQUEST_TAG_INFO + SUCCESS: {
      const { tagInfo } = action.payload;

      return {
        ...state,
        tagInfo,
      };
    }

    case REQUEST_STORIES_BY_TAG:
    case CLEAR_TAG_INFO: {
      return {
        ...state,
        tagInfo: null,
      };
    }
    default:
      return state;
  }
}
