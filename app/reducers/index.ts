import { combineReducers } from 'redux';

import app, { AppState } from './app';
import oauth from './oauth';
import stories from './stories';
import dialog, { DialogState } from './dialog';
import tags, { TagsState } from './tags';
import users from './users';

export interface ApplicationState {
  app: AppState;
  tags: TagsState;
  dialog: DialogState;
}

const rootReducer = combineReducers<ApplicationState>({
  app,
  oauth,
  stories,
  tags,
  dialog,
  users,
});

export default rootReducer;
