import React, { FC } from 'react';
import { Link } from 'react-router-dom';
import { useSelector } from 'react-redux';

const TagInfo: FC<{ name: string }> = ({ name }) => {
  const tag = useSelector((state: RootState) => state.tags.tagInfo);

  return (
    <div className="content header">
      <div className="row">
        <div>
          <h1>
            <Link className="tag-content__back-link" to="/tags/">
              Истории по теме
            </Link>
            :&nbsp;
            <span style={{ color: 'red' }}>
              {name}
            </span>
          </h1>
          <div className="text">
            <p>{tag?.description}</p>
          </div>
        </div>
        <div className="tag-icon-aside">
          <img style={{ width: '100%' }} src={tag?.image} alt="" />
        </div>
      </div>
    </div>
  );
};

export default TagInfo;
