import React, { FC, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useUnmount, usePrevious } from 'react-use';
import { useRouteMatch } from 'react-router-dom';

import withDataPreload from '@utils/preload';
import ac from '@utils/ac';
import Story from '../../components/Content/Story';
import {
  REQUEST_TAG_INFO,
  REQUEST_STORIES_BY_TAG,
  CLEAR_TAG_INFO,
} from '../../actions';

import TagInfo from './parts/tag-info';
import { Tag } from './types';

import './styles.css';

const TagContent: FC = () => {
  const dispatch = useDispatch();
  const match = useRouteMatch<{ tag: string }>();

  const { tag } = match.params;
  const stories = useSelector((state) => state.tags.stories);
  const prevTag = usePrevious(tag);

  useUnmount(() => dispatch(ac(CLEAR_TAG_INFO)));

  useEffect(() => {
    if (!prevTag) {
      return;
    }

    if (tag !== prevTag) {
      dispatch(ac(REQUEST_STORIES_BY_TAG, { tag }));
      dispatch(ac(REQUEST_TAG_INFO, { tag }));
    }
  }, [tag, prevTag]);

  if (!tag && stories.length === 0) {
    return null;
  }

  return (
    <div className="tag-content">
      <TagInfo name={tag} />
      {/* TODO: use story-list */}
      {stories.map((story) => (
        <Story
          story={story}
          key={story.uID}
          activeTag={tag}
        />
      ))}
    </div>
  );
};

export default withDataPreload({
  fetch: (props) => {
    const { dispatch } = props;
    const { tag } = props.match.params;

    dispatch(ac(REQUEST_STORIES_BY_TAG, { tag }));
    dispatch(ac(REQUEST_TAG_INFO, { tag }));
  },
}, TagContent);
