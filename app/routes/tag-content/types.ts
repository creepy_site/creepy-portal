export interface Tag {
  name: string;
  description: string;
  image: string;
}
