import React, { FC, useEffect } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useUnmount, usePrevious } from 'react-use';
import { useRouteMatch } from 'react-router-dom';

import NotFound from '@routes/not-found';

import withDataPreload from '@utils/preload';
import ac from '@utils/ac';

import StoryList from '@components/Content/StoryList';

import {
  CLEAR_STORIES,
  REQUEST_STORIES,
  getStoryLoadingState,
} from '../actions';

interface Props {
  helloMessage: boolean;
}

const TOKEN = 'stories';

const StoriesPage: FC<Props> = ({ helloMessage }) => {
  const dispatch = useDispatch();
  const match = useRouteMatch();

  useUnmount(() => dispatch(ac(CLEAR_STORIES)));

  const stories = useSelector((state) => state.stories);
  const loading = useSelector(getStoryLoadingState);
  const { page = '1' } = match.params;

  const prevPage = usePrevious(page);

  useEffect(() => {
    if (!prevPage) {
      return;
    }

    if (page !== prevPage) {
      dispatch(ac(REQUEST_STORIES, { query: TOKEN, offset: page }));
    }
  }, [page, prevPage]);

  if (!loading && !stories) {
    return <NotFound />;
  }

  if (stories.entries.length === 0) {
    return null;
  }

  return (
    <StoryList
      stories={stories.entries}
      helloMessage={helloMessage}
      showPagination={stories.showPagination}
      storiesTotal={stories.count}
      currentPage={parseInt(page, 10)}
      token={TOKEN}
    />
  );
};

export default withDataPreload({
  fetch: ({ dispatch, match }) => {
    const page = match?.params.page || '1';

    dispatch(ac(REQUEST_STORIES, { query: TOKEN, offset: page }));
  },
}, StoriesPage);
