import React, { FC, useEffect, useState } from 'react';
import { useSelector } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';

import withDataPreload from '@utils/preload';
import ac from '@utils/ac';

import { 
  REQUEST_ALL_TAGS
} from '@actions';

import './style.pcss';

const TagsPage: FC = ()  => {
  const list = useSelector((state) => state.tags.list);

  const [query, setQuery] = useState('');
  const [filteredTags, setFilteredTags] = useState([]);

  useEffect(() => {
    setFilteredTags(list);
  }, [list]);

  const onQueryChange = (event: React.ChangeEvent<HTMLInputElement>) => {
    const { value } = event.target;
    const newTagsSet = list
      .filter((tag: string) => tag.indexOf(value) !== -1);

    setQuery(value);
    setFilteredTags(newTagsSet)
  }

  return (
    <div className='content wide'>
      <Helmet><title>Список тем</title></Helmet>
      <div className='tags-list'>
      <div>
        <h1>Темы историй:</h1>
        <input
          type='text'
          className='tag-list__search'
          value={query}
          onChange={onQueryChange}
          placeholder='Поиск по тегам'
        />
      </div>
      <div className='taglist'>
        { filteredTags.map((tag) => (
          <Link key={tag} to={`/tags/${tag}`} className='tag'>{ tag }</Link>
        ))}
      </div>
    </div>
  </div>
  );
}

export default withDataPreload({
  fetch: ({ dispatch }) => dispatch(ac(REQUEST_ALL_TAGS)),
}, TagsPage);
