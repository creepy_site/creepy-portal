import React, { FC } from 'react';
import { useDispatch } from 'react-redux';

import { SHOW_DIALOG } from '@actions';
import ac from '@utils/ac';

import moment from 'moment';
import 'moment/locale/ru';

import { AVATAR_UPLOAD_MODAL } from '../../../components/AvatarUploadModal/AvatarUploadModal';

const Header: FC = (props) => {
  const {
    isCurrentUser,
    requestUser,
  } = props;
  const firstPubDate = requestUser.firstPubDate;
  const accountImage = requestUser.accountImage;

  const dispatch = useDispatch();

  return (
      <div className="content wide user-page--header">
        <div className="account-info">
          <div className="account-portrait">
            { accountImage
              ? <img
                className="account-portrait-image"
                src={accountImage}
              />
              : <img
                className="account-portrait-image account-portrait-question"
                src="/images/question-sign.svg"
              />
            }
            {isCurrentUser
              && <div
                className="account-portrait--upload-btn"
                onClick={() => dispatch(ac(SHOW_DIALOG, { content: AVATAR_UPLOAD_MODAL }))}>
                Загрузить
              </div>
            }
          </div>
        </div>
        <div className="user-page--header-description">
          <h1>{requestUser.username}</h1>
          <p>
            <i className="fa fa-star"/>
            Рейтинг: {requestUser.rating}
          </p>
          <p>
            <i className="fa fa-clock-o"/>
            Дата регистрации: {moment(requestUser.dateRegister).format('DD MMMM YYYY')}
          </p>
          {firstPubDate
            && <p>
              <i className="fa fa-book"/>
              Первая публикация: {moment(firstPubDate).format('DD MMMM YYYY')}
            </p>
          }
        </div>
      </div>
  );
};


export default Header;
