import React, { FC, Fragment } from 'react';
import { useSelector } from 'react-redux';
import { Helmet } from 'react-helmet';
import { Link } from 'react-router-dom';
import classNames from 'classnames';

import withDataPreload from '@utils/preload';
import ac from '@utils/ac';

import { 
  REQUEST_USER_INFO,
  REQUEST_USER_PUB_INFO,

  // TODO: get rid
  getCurrentUserUsername,
} from '@actions';

import Header from './parts/header';

const STATUSES = [
  'В очереди на рассмотрение',
  'Отклонена',
  'В рассмотрении',
  'Опубликована',
];

const getNoPublicationsMessage = (isCurrentUser) => (isCurrentUser
  ? <p>Вы пока не сделали ни одной публикации</p>
  : <p>Автор пока не сделал ни одной публикации</p>
);

const getPublicationItem = (publication, isCurrentUser) => {
  // TODO: replace by dict
  const STATUS_PUBLISHED = 3;
  const uID = publication.uID;
  const title = publication.title;
  const status = publication.status;
  const likesCount = publication.likesCount;
  const commentsCount = publication.commentsCount;
  const isPublishedStory = status === STATUS_PUBLISHED;

  return (
    <li key={uID} className="publication-item">
      <div>
        <Link to={`/story/${uID}`}>
          <b>{title}</b>
        </Link>
      </div>
      <div className="publication-info">
        {isCurrentUser && !isPublishedStory
          && <span
            className={classNames('publication-field-status', {
              'publication-field-status_rejected': status === 1,
            })}
          >
            {STATUSES[status]}
          </span>
        }
        { isPublishedStory
          && <Fragment>
            <span className="publication-field-comments"><i className="fa fa-heart" /> {likesCount}</span>
            <span className="publication-field-likes"><i className="fa fa-comments" /> {commentsCount}</span>
          </Fragment>
        }
      </div>
    </li>
  );
};

const UserPage: FC = ()  => {
  const currentUser = useSelector(getCurrentUserUsername);
  const requestedUser = useSelector((state) => state.users.user);
  const pubInfo = useSelector((state) => state.users.userPubInfo);

  if(!requestedUser) {
    return null;
  }

  const isCurrentUser = currentUser === requestedUser.username;

  return (
    <div className="wrap">
    <Helmet>
      <title>{requestedUser.username} профиль</title>
      <meta name='Description' content="В данном разделе отображается информация о профиле пользователя"/>
    </Helmet>
    <Header
      isCurrentUser={isCurrentUser}
      requestUser={requestedUser}
    />
    <div className="content wide white padding">
      { pubInfo.length === 0
        ? getNoPublicationsMessage(isCurrentUser)
        : <div>
          <h2>{isCurrentUser ? 'Ваши публикации:' : 'Публикации:'}</h2>
          {isCurrentUser && (
            <p>Список историй, ожидающих публикации и отклоненных виден только вам.</p>
          )}
          <ul className="publication-list">
            {pubInfo.map((item) => getPublicationItem(item, isCurrentUser))}
          </ul>
        </div>
      }
    </div>
  </div>
  );
}

export default withDataPreload({
  fetch: ({ dispatch, match }) => {
    const { username } = match.params;

    dispatch(ac(REQUEST_USER_INFO, { username }));
    dispatch(ac(REQUEST_USER_PUB_INFO, { username }));
  },
}, UserPage);
