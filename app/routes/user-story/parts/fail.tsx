import React, { FC } from "react";

export const Fail: FC = () => (
  <div className="user-story">
    <p>Во время добавления истории возникли некоторые технические пробелмы.</p>
    <p>Повторите пожалуйста запрос чуть позже.<br/></p>
    <p>Если данная ошибка появится снова, напишите нам на <a href="mailto:support@scary-stories.ru">почту</a>.</p>
  </div>
);
