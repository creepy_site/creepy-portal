import React, { FC } from "react";

export const Success: FC = () => (
  <div className="user-story">
  <p>Вашу историю в скором времени рассмотрят и если она соответствует требованиям - опубликуют.</p>
  <p>Спасибо за Ваш вклад.</p>
</div>
);
