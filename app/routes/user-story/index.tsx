import React, { FC, useState } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { Helmet } from 'react-helmet';

import ac from '@utils/ac';

import {
  getUserStoryState,
  REQUEST_USER_ADD_STORY,
} from '../../actions';

import { Success } from './parts/success';
import { Fail } from './parts/fail';

import './style.css';

// import Recaptcha from 'react-recaptcha';

const UsersStory: FC = () => {
  const dispatch = useDispatch();

  const [title, setTitle] = useState('');
  const [content, setContent] = useState('');

  const handleSubmit = (event) => {
    const { target: form } = event;
    event.preventDefault();

    const focusNode = (name: string) => form.querySelector(`input[name=${name}]`).focus();

    if (!title.trim()) {
      return focusNode('title');
    }

    if (!content.trim()) {
      return focusNode('content');
    }

    dispatch(ac(REQUEST_USER_ADD_STORY, { title, content }));
  }

  const {
    loading,
    success,
    fail,
  } = useSelector(getUserStoryState);

  return (
    <div className="wrap">
      <Helmet>
        <title>Новая история</title>
        <meta name='Description' content="Возможность поделиться своей историей." />
      </Helmet>
      <div className="content wide white padding">
        <form id="add-story" onSubmit={handleSubmit}>
          <h1>Новая история</h1>
          <p>Если вы хотите поделиться с посетителями сайта своей страшной историей, напишите её прямо здесь.</p>
          { !loading && !success && !fail && (
            <div className="user-story">
              <div className="user-story__row">
                <p>Название:</p>
                <input id="input-name" type="text" placeholder="Крипота" name="title" value={title} onChange={({ target }) => setTitle(target.value)}/>
              </div>
              <div className="user-story__row">
                <p>Текст истории:</p>
                <textarea id="input-content" placeholder="Давным давно..." name="content" value={content} onChange={({ target }) => setContent(target.value)}/>
              </div>
              <p>Отправляя историю, вы соглашаетесь, что она пройдёт через редактирование и может быть существенно изменена.</p>
              <button className="submit-btn submit-btn--add-story" type="submit">Отправить</button>
            </div>
          )}
          { success && <Success /> }
          { fail && <Fail /> }
        </form>
      </div>
      <div style={{ clear: 'both' }}></div>
    </div>
  );
}

export default UsersStory;
