import React, { FC } from 'react';
import { useDispatch, useSelector } from 'react-redux';
import { useUnmount } from 'react-use';

import NotFound from '@routes/not-found';

import Story from '@components/Content/Story';
import withDataPreload from '@utils/preload';
import ac from '@utils/ac';

import {
  CLEAR_STORY,
  REQUEST_STORY,
  getStory,
  getStoryLoadingState,
} from '../actions';

const token = 'story';

const RandomPage: FC = () => {
  const dispatch = useDispatch();

  useUnmount(() => dispatch({ type: CLEAR_STORY }));

  const story = useSelector(getStory);
  const loading = useSelector(getStoryLoadingState);

  if (!loading && !story) {
    return <NotFound />;
  }

  if (!story) {
    return null;
  }

  return (
    <Story
      verbose
      story={story}
    />
  );
};

export default withDataPreload({
  fetch: ({ dispatch, match }) => {
    const { id } = match.params;

    dispatch(ac(REQUEST_STORY, { token, id }));
  },
}, RandomPage);
