import React, { FC } from 'react';
import Status from '../components/status';

const NotFound: FC = () => (
  <Status code={404}>
    <div className="content text wide white padding"  style={{position:"relative"}}>
      <h1>Такой страницы не существует <img src="/images/spooky-dark.png" className="ghost-dark-icon"/></h1>
      <p>Но это не значит, что всё потеряно. У нас много историй одна страшнее другой!</p>
      <p style={{marginTop:"15px"}}>Чтобы прочитать последние выложенные истории, загляните в раздел <a href="/stories/1" title="Новые">Новые</a>.
      В разделе <a href="/scary/1" title="Страшные">Страшные</a> собраны самые зловещие истории.
      Если не знаете, что вы хотите, советуем <a href="/random" title="Случайная история">Случайную историю</a>.</p>
      <p style={{marginTop:"15px"}}>А быть может вы ищите что-то определенное. Для этого загляните в в <a href="/tags" title="Поиск по темам"> поиск по определенной теме</a>.</p>
    </div>
  </Status>
);

export default NotFound;
