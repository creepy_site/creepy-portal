import { AxiosInstance } from 'axios';

export function fetchStory(transport: AxiosInstance, token: string, id: string) {
  const url = `/stories/${token === 'random' ? token : id}`;

  return transport(url)
    .then((response) => {
      if (response.status !== 200) {
        return null;
      }
      return response.data;
    })
    // TODO: should to make normal logging
    .catch((e) => {
      console.log('something going wrong', e);
    });
}

export function fetchStoriesCount(transport: AxiosInstance) {
  const url = '/stories/count';

  return transport(url)
    .then((response) => response.data.count)
    // TODO: should to make normal logging
    .catch((e) => {
      console.log('something going wrong', e);
    });
}

export function fetchStories(transport: AxiosInstance, query: string, offset: string) {
  const token = (query === 'stories' ? 'latest' : 'scary');

  const url = `/stories/${token}/${offset}`;

  return transport(url)
    .then((response) => response.data)
    .catch((e) => {
      console.log('something going wrong', e);
    });
}

export function fetchStoriesByTag(transport: AxiosInstance, tag: string) {
  const url = encodeURI(`/stories/tag/${tag}`);

  return transport(url)
    .then((response) => response.data)
    .catch((e) => {
      console.log('something going wrong', e);
    });
}

export function requestStoryLike(transport: AxiosInstance, uID: string, action: string) {
  const url = `/stories/${action}/${uID}`;

  return transport.post(url)
    .catch((e) => {
      console.log('something going wrong', e);
    });
}

export function requestInitialData(transport: AxiosInstance, uIDs: string) {
  const url = '/stories/initial';

  return transport.post(url, { uIDs })
    .then((response) => response.data)
    .catch((e) => {
      console.log('something going wrong', e);
    });
}

export function appendComment(transport: AxiosInstance, uID: string, content: string) {
  const url = '/comments/';

  return transport.put(url, { uID, content })
    .then((response) => response.data)
    .catch((e) => {
      console.log('something going wrong', e);
    });
}
