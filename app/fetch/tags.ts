import { AxiosInstance } from 'axios';

export function fetchAllTags(transport: AxiosInstance) {
  const URL = '/tags/all';

  return transport(URL)
    .then((response) => response.data)
    .catch(console.log);
}

export function fetchTagInfo(transport: AxiosInstance, tag: string) {
  const URL = encodeURI(`/tags/info/${tag}`);

  return transport(URL)
    .then((response) => response.data)
    .catch((err) => {
      if (!err.response || err.status > 500) {
        console.error(err);
      }
    });
}
