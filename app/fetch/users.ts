import { AxiosInstance } from 'axios';

/*
_id(pin):"5a5be2eee9a1142a648d446c"
accountImage(pin):"/data/avatars/user-0450bd7c-e5bf-48bf-84d6-525065361602.png"
dateRegister(pin):"2017-02-02T20:56:26.117Z"
hidden(pin):false
scope(pin):"user"
username(pin):"user"
rating(pin):0
*/

export function requestUserInfo(transport: AxiosInstance, username: string) {
  const URL = `/users/${username}`;

  return transport.get<number>(URL)
    .then((response) => response.data)
    .catch((e) => {
      console.log('something went wrong', e);
    });
}

export function postStory(transport: AxiosInstance, title: string, content: string) {
  const url = '/stories/new';

  return transport.put(url, { title, content })
    .then((response) => response.data)
    // TODO: should to make normal logging
    .catch((e) => {
      console.log('something going wrong', e);
    });
}

export function requestPubInfoOfUser(transport: AxiosInstance, username: string) {
  const url = `/stories/byUser/${username}`;

  return transport(url)
    .then((response) => response.data)
    .catch((e) => {
      console.log('something went wrong', e);
    });
}

export function uploadAvatarImage(transport: AxiosInstance, file: any) {
  const URL = '/user/upload';
  const opts = {
    headers: {
      'Content-Type': 'multipart/form-data',
    },
  };
  const formData = new FormData();

  formData.append('avatar', file);

  return transport.post(URL, formData, opts)
    .then((response) => response.data)
    // TODO: should to make normal logging
    .catch((e) => {
      console.log('something going wrong', e);
    });
}
