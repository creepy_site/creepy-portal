import { AxiosInstance } from 'axios';

import config from '../../config';

const { HOST } = config;

export function fetchAuth(transport: AxiosInstance, login: string, password: string) {
  const URL = `${HOST}/actions/oauth/token`;

  return transport.post(URL, { login, password })
    .then((response) => response.data)
    // TODO: should to make normal logging
    .catch(console.log);
}

export function fetchRegister(transport: AxiosInstance, data: any) {
  const URL = `${HOST}/actions/oauth/register`;

  return transport.post(URL, { data })
    .then((response) => response.data)
    .catch(console.log);
}

export function requestLogout(transport: AxiosInstance) {
  const URL = `${HOST}/actions/oauth/logout`;
  delete transport.defaults.headers.common.Authorization;

  return transport
    .post(URL)
    .catch(console.log);
}
