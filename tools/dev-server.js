const fs = require('fs');
const path = require('path');
const chalk = require('chalk');
const webpack = require('webpack');
const nodemon = require('nodemon');

const webpackConfig = require('../webpack.config');

const missingFile = (filename) => {
  console.error(chalk.red(`Missing ${filename}.ts file. You must create this file by type from ./types/${filename}.ts`));
};

const run = async () => {
  if (!fs.existsSync('./config.ts')) {
    missingFile('config');
    process.exit(1);
  }

  if (!fs.existsSync('./config-server.ts')) {
    missingFile('config-server');
    process.exit(1);
  }

  const compilerServer = webpack(webpackConfig({
    server: true,
    production: false,
  }));

  const compilerClient = webpack(webpackConfig({
    server: false,
    production: false,
  }));

  // запуск компиляции серверной части
  compilerClient.watch({}, (err) => {
    if (err) {
      console.error(err);
    }
  });

  // запуск компиляции серверной части
  compilerServer.watch({
    // poll: 1000,
  }, (err) => {
    if (err) {
      console.error(err);
    }
  });

  // ожидание завершения компиляции серверной части
  try {
    console.log('Compiling server...');

    await new Promise((resolve, reject) => {
      compilerServer.hooks.done.tap('DevServer', (stats) => {
        if (stats.hasErrors()) {
          console.error(stats.compilation.errors.join('\n'));

          return reject(Error('Compilation failed'));
        }

        console.log('Server compiled successfully');
        resolve();
      });
    });
  } catch (err) {
    console.error(err);
  }

  // запуск nodemon для перезапуска серверной части при её изменении
  console.log('Run server...');
  const serverScript = nodemon({
    script: path.resolve(__dirname, '..', 'bin', 'server.bundle.js'),
    watch: [
      path.resolve(__dirname, '..', 'bin'),
    ],
    stdout: true,
    // delay: 500,
  });

  serverScript.on('readable', function handleStream() {
    this.stdout.on('data', (data) => {
      console.log(String(data).trim());
    });

    this.stderr.on('data', (data) => {
      console.error(String(data).trim());
    });
  });

  serverScript.on('restart', (files) => {
    console.log(chalk.green('Files changed:', files));
    console.log(chalk.green.bold('Restarting server...'));
  });

  serverScript.on('quit', () => {
    process.exit();
  });

  serverScript.on('error', () => {
    console.error('An error occured. Exiting');
    process.exit(-1);
  });
};

run();
