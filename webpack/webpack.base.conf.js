const { resolve } = require('path');
const webpack = require('webpack');
const path = require('path');
const chalk = require('chalk');
const TsconfigPathsPlugin = require('tsconfig-paths-webpack-plugin');
const MiniCssExtractPlugin = require('mini-css-extract-plugin');
const nodeExternals = require('webpack-node-externals');

const ENV = process.env.NODE_ENV || 'development';

console.log(`${chalk.cyan('Webpack building for')}: ${chalk.bold.red(ENV)}`);

module.exports = (env) => {
  console.log(env);
  const use = [];

  if (env.server) {
    use.push(
      'style-loader',
      {
        loader: 'css-loader',
        options: {
          url: false,
          import: false,
          modules: {
            exportOnlyLocals: true,
          },
        },
      },
    );
  } else {
    use.push(
      env.production ? MiniCssExtractPlugin.loader : 'style-loader',
      {
        loader: 'css-loader',
        options: {
          url: false,
          import: false,
          modules: { auto: true },
        } 
      },
      {
        loader: 'postcss-loader',
        options: {
          postcssOptions: {
            plugins: [
              require('postcss-import')({
                path: [
                  'app',
                    resolve(__dirname, '..', 'app')
                ]
              }),
              'postcss-nested',
              'postcss-simple-vars',
            ],
          },
        },
      },
    );
  }

  const output = {
    path: path.resolve(__dirname, env.server ? '../bin' : '../public/js'),
    filename: env.server ? 'server.bundle.js' : 'main.bundle.js',
  };

  const entryServer = [path.resolve('./server/server')];
  const entryClient = [
    // 'react-hot-loader/patch',
    // 'webpack-hot-middleware/client',
    path.resolve('./app/main.tsx'),
  ];

  return {
    watch: true,
    target: env.server ? 'node' : 'web',
    ...(env.server ? {
      externals: [nodeExternals()],
    } : {}),
    mode: 'development',
    entry: env.server ? entryServer : entryClient,
    resolve: {
      extensions: ['.js', '.jsx', '.ts', '.tsx', '.json', '.css', '.scss'],
      plugins: [
        new TsconfigPathsPlugin({
          configFile: './tsconfig.json',
          logLevel: 'info',
          extensions: ['.ts', '.tsx'],
          mainFields: ['browser', 'main'],
        }),
      ],
    },
    output,
    module: {
      rules: [
        {
          test: /\.js$/,
          exclude: /node_modules/,
          use: {
            loader: 'babel-loader',
          },
        },
        {
          test: /\.css$/i,
          use,
        },
        {
          test: /\.pcss$/i,
          use,
        },
        {
          test: /(\.tsx|\.ts)$/,
          use: [{
            loader: 'ts-loader',
            options: {
              transpileOnly: true,
            },
          }],
        },
      ],
    },
    plugins: [
      new webpack.IgnorePlugin(/^\.\/locale$/, /moment$/, /axios$/),
      new MiniCssExtractPlugin({
        // filename: false ? '[name].[contenthash].css' : '[name].css',
        filename: '../stylesheets/[name].css',
      }),
      new webpack.DefinePlugin({
        __CLIENT__: !env.server,
        __SERVER__: env.server,
      }),
    ],
    stats: { colors: true },
  };
};
