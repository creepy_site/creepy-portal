const FriendlyErrors = require('friendly-errors-webpack-plugin');

module.exports = () => ({
  devtool: 'source-map',
  plugins: [
    new FriendlyErrors(),
  ],
});
