import { Environment } from './env';

export interface ConfigPart {
  API_HOST: string,
  HOST: string,
  SSR: boolean,
}

export type Config = {
  [key in Environment]: ConfigPart
};
