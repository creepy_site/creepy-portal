/* eslint-disable @typescript-eslint/naming-convention */

declare global {
  declare const __SERVER__: boolean;
  declare const __CLIENT__: boolean;

  interface Window {
    __SSR__: boolean;
    __ENV__: string;

    [key: string]: any;
  }
}

import 'react-redux';
import { ApplicationState } from '@reducers/index';
declare module 'react-redux' {
  interface DefaultRootState extends ApplicationState {}
} 
