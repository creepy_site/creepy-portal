import { ConnectionOptions } from 'mongoose';
import { Environment } from './env';

export type ServerConfig = {
  [key in Environment]: {
    database: {
      uri: string;
      options: ConnectionOptions;
    }
    CLIENT_ID: string;
    CLIENT_SECRET: string;
    SESSION_SECRET: string;
  }
};
