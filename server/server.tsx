import http from 'http';
import io from 'socket.io';
import express, { Request } from 'express';
import compression from 'compression';
import morgan from 'morgan';
import mongoose from 'mongoose';
import bodyParser from 'body-parser';
import path from 'path';
import swig from 'swig-templates';
import device from 'express-device';
import session from 'express-session';
import React from 'react';
import { renderToString } from 'react-dom/server';
import { Provider } from 'react-redux';
import { StaticRouter } from 'react-router-dom';
import proxy from 'express-http-proxy';
import { parse } from 'url';
import axios from 'axios';
import useragent from 'express-useragent';
import { Context as ResponsiveContext } from 'react-responsive';

import config from '../config';
import serverConfig from '../config-server';
import configureStore from '../app/utils/configureStore';
import Application from '../app/components/App/application';
import rootSaga from '../app/sagas';
import { REQUEST_AUTH, SET_LOGO_NUM } from '../app/actions';
import { SUCCESS } from '../app/actions/baseActions';

import tokenManager from './tokenManager';
import apiOauthRouter from './api/oauth';

const MongoStore = require('connect-mongo')(session);

const LOGOS_COUNT = 6;
const {
  SSR,
  API_HOST,
} = config;
const {
  database,
  SESSION_SECRET,
} = serverConfig;

const app = express();

tokenManager.getAppToken();

// if (ENV === 'development') {
//   /* eslint-disable */
//   const webpack = require('webpack');
//   const webpackDevMiddleware = require('webpack-dev-middleware');
//   const webpackConfig = require('../webpack.config.babel.js');
//   /* eslint-enable */
//
//   const compiler = webpack(webpackConfig);
//   app.use(webpackDevMiddleware(compiler, {
//     publicPath: path.join(__dirname, '../', 'public'),
//     writeToDisk: filePath => filePath.endsWith('bundle.js') || filePath.endsWith('bundle.js.map'),
//   }));
// }

mongoose.connect(database.uri, database.options);
mongoose.connection.on('error', () => console.error('Error: Could not connect to MongoDB. Did you forget to run `mongod`?'));

mongoose.connection.on('disconnected', () => {
  console.info('Mongoose has disconnected');
  console.info('Next auth attempt after 5 sec');
  setTimeout(() => {
    mongoose.connect(database.uri, database.options);
  }, 2000);
});

/* TODO: change to getEnv from utils */
app.set('port', process.env.PORT || 3000);
app.enable('trust proxy');
app.use(compression());
app.use(morgan('dev'));
app.use(useragent.express());
app.use(session({
  secret: SESSION_SECRET,
  resave: true,
  saveUninitialized: true,
  store: new MongoStore({ mongooseConnection: mongoose.connection })
}));

app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: true }));
// app.use(favicon(path.join(__dirname, 'public', 'favicon.png')));
app.use(express.static(path.join(__dirname, '../', 'public')));
app.set('view engine', 'html');
app.use(device.capture());

/* Middleware section */
const cors = require('./middleware/cors');
const debug = require('./middleware/debug');

app.use(cors);
app.use(debug);

/* proxy static to api server users files */
app.use('/data/', proxy(API_HOST, {
  proxyReqPathResolver: (req: Request) => `/data/${parse(req.url).path}`,
}));

/*
  Site internal api and server side proxy
  Example flow:
    client auth -> [server internal auth] -> api auth -> [server] -> client
*/
app.use('/actions/', apiOauthRouter);

const render = (store, url, isMobile) => {
  const context = {};

  const MOBILE_WITH = 415;
  const DESKTOP_WITH = 1400;

  const html = renderToString(
    <ResponsiveContext.Provider value={{ width: isMobile ? MOBILE_WITH : DESKTOP_WITH }}>
      <Provider store={store}>
        <StaticRouter location={url} context={context}>
          <Application />
        </StaticRouter>
      </Provider>
    </ResponsiveContext.Provider>,
  );

  return [context, html];
};

app.use((req, res) => {
  const transport = axios.create({
    baseURL: config.API_HOST,
  });

  const usrSession = req.session;
  const store = configureStore(transport);
  let sagasPromise;

  if (usrSession.oauth) {
    transport.defaults.headers.common.Authorization = `Bearer ${usrSession.oauth.token}`;
  }

  if (SSR) {
    const { isMobile } = req.useragent;

    sagasPromise = store.runSaga(rootSaga).done;
    render(store, req.url, isMobile);
  }

  /* if user already have session */
  if (usrSession.oauth) {
    store.dispatch({
      type: REQUEST_AUTH + SUCCESS,
      payload: { authData: usrSession.oauth },
    });
  }

  store.dispatch({
    type: SET_LOGO_NUM,
    payload: { number: Math.round(Math.random() * LOGOS_COUNT) },
  });

  if (!SSR) {
    const snapshot = Buffer.from(JSON.stringify(store.getState()), 'utf-8').toString('base64');

    const page = swig.renderFile('views/index.html', {
      ssr: false,
      env: process.env.NODE_ENV,
      snapshot,
    });

    return res.status(200).send(page);
  }

  sagasPromise.then(() => {
    const { isMobile } = req.useragent;

    /* dispatch oauth event if data exist */
    const [context, html] = render(store, decodeURIComponent(req.url), isMobile);

    console.log(context);
    // const helmet = Helmet.renderStatic();
    const snapshot = Buffer.from(JSON.stringify(store.getState()), 'utf-8').toString('base64');

    const page = swig.renderFile('views/index.html', {
      ssr: true,
      env: process.env.NODE_ENV,
      snapshot,
      html,
      // helmet,
    });

    res.status(context.status || 200).send(page);
  });
  store.close();
});

console.log('Starting creepy-web with NODE_ENV: ', process.env.NODE_ENV);
const server = http.createServer(app);

server.listen(app.get('port'), () => {
  console.log(`creepy-web listening on port ${app.get('port')}`);
});

app.use((err, req, res) => {
  console.log('\nThis error shows when express app has bad configuration');
  console.log('-OR-');
  console.log('In case of serious backend error.\n');
  console.log(err);

  const page = swig.renderFile(
    `${__dirname}/../views/error_dev.html`,
    { error: err, stack: err.stack },
  );

  res.status(err.status || 500).send(page);
});

/* sockets test section */
const ws = io(server);

let onlineUsers = 0;
ws.sockets.on('connection', (socket) => {
  onlineUsers += 1;

  ws.sockets.emit('onlineUsers', { onlineUsers });

  socket.on('disconnect', () => {
    onlineUsers -= 1;
    ws.sockets.emit('onlineUsers', { onlineUsers });
  });
});
